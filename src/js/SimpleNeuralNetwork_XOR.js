/**
* Simple neural network for XOR problem with back propagation
*/
import Neuron from "./ANN_utils/Neuron";
import Layer, {valueCheck} from "./ANN_utils/Layer";

/**
 * Network network with back propagation to solve XOR problem
 */
const epochs = 50000;
const learningRate = 0.1;

const finalOut = [];


/**
 * ALGORITHM
 */
const SimpleNeuralNetworkXOR = () => {

    const learningValues = [[0,0,0],[0,1,1],[1,0,1],[1,1,0]];

    const hiddenLayer = new Layer([
        new Neuron([0.15,0.20], 0.35, learningRate),
        new Neuron([0.25,0.30], 0.35, learningRate)
    ]);

    const outputLayer = new Layer([
       new Neuron([0.40,0.45], 0.50, learningRate)
    ]);


    // LEARNING PHASE
    for(let i = 0; i < epochs; i++){

        for(let value of learningValues){
            const outputValue = outputLayer.run(hiddenLayer.run([value[0], value[1]]));
            hiddenLayer.calculateNormalError(outputLayer.calculateOutputError(value[2]));
        }
    }


    // CLASSIFY PHASE
    console.log("-- XOR CLASSIFY --");
    console.log(`| x | y | XOR |`);
    console.log(`----------------`);
    for(let value of learningValues) {
        const outputValue = outputLayer.run(hiddenLayer.run([value[0], value[1]]));
        finalOut.push(outputValue[0]);
        console.log(`| ${value[0]} | ${value[1]} | ${Math.round(valueCheck(value, outputValue, finalOut))} |`);
        // console.log(`----------------`);
    }
        console.log(`----------------`);
};

SimpleNeuralNetworkXOR();