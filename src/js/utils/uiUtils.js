export const emptyInputValidation = (e) => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            return true;
        } else {
            alert("Set number value, por favor.");
        }
    }
};

export const emptyInputValidationWithRange = (e, min, max) => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value) || e.target.value == min) {
            let value = parseInt(e.target.value);

            if(value <= max) {
                return true;
            } else {
                alert(`Set value ${min} - ${max}, por favor.`);
                return false;
            }
        } else {
            alert("Set number value, por favor.");
            return false;
        }
    } else {
        return true;
    }
};