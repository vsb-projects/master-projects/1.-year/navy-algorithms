import Plotly from "plotly.js-dist";

let CANVAS = document.querySelector('#canvas');


export function renderHeatmap(dataArray) {
    const data = [
        {
            z: dataArray,
            type: 'heatmap'
        }
    ];

    Plotly.newPlot(CANVAS, data);
}

export function renderGridOnCanvas(dataArray, canvas, title = "") {

    const colorscaleValues = [
        [0, 'rgb(165,163,167)'],
        [1, '#2c4a73']
    ];

    const layout = {
        title: title,
        margin: {
            t: 20,
            r: 20,
            b: 20,
            l: 20
        },
        showlegend: false,
        // with: 700,
        // height: 700,
        // autosize: false,
        xaxis: {
            ticks: '',
            // range: [0, 100],
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            showticklabels: false
        },
        yaxis: {
            ticks: '',
            // range: [0, 100],
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            showticklabels: false
        }
    };

    const data = [
        {
            z: dataArray,
            type: 'heatmap',
            showscale: false,
            colorscale: colorscaleValues
        }
    ];

    Plotly.newPlot(canvas, data, layout);
}