import Point from "../models/Point";
import Trace from "../models/Trace";
import Plotly from "plotly.js-dist";

let CANVAS = document.querySelector('#canvas');

export const TraceModeTypes = {
  markers: "markers",
  lines: "lines",
  markersAndLines: "markers+lines"
};

export const TraceTypeTypes = {
  scatter: "scatter"
};

export const generatePoints = (n, pointsRange) => {
    const pointsArray = [];

    for(let i = 0; i < n; i++){
        let x = Math.floor(Math.random() * pointsRange);
        let y = Math.floor(Math.random() * pointsRange);

        let point = new Point(x,y,i);
        pointsArray.push(point);
    }
    // console.log(pointArray);

    return pointsArray;
};

/**
 * RENDER
 */
export function render2DGraph(canvas, traces, title, width, height) {
    let data = [];

    for (let t of traces) {
        data.push(t.renderTrace());
    }

    // let data = [trace.renderTrace()];

    let layout = {
        margin: {t:50, l:30, b:30, r:20},
        title: title,
        width: width,
        height: height,
        xaxis: {
            // range: [2, 4],
        //     autorange: true,
            showgrid: false,
            zeroline: false,
            // showline: true,
            // showticklabels: false
        },
        yaxis: {
        //     // range: [0, 100],
        //     autorange: true,
            showgrid: false,
            zeroline: false,
            // showline: true,
            // showticklabels: false
        }
    };

    // render
    // Plotly.plot(canvas, data, layout);
    Plotly.newPlot(canvas, data, layout);
}

export function deleteTraces(canvas) {
    Plotly.deleteTraces(canvas, 0);
}

export function animatePoints(canvas,points) {
    Plotly.animate(canvas, {
        data: [{
            name: "ACO algorithm",
            mode: "markers",
            markers: {size: 1},
            x: points.map(p => p.x),
            y: points.map(p => p.y),
        }]
    },
        {
            transition: {
                duration: 500,
                easing: 'cubic-in-out'
            },
            frame: {
                duration: 500
            }
    });
}
