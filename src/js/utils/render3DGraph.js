import {Point3D} from "../models/Point";
import Plotly from "plotly.js-dist";
import {Create2DArray, Create2DArrayWithFill} from "./utilFunctions";

const CANVAS = document.querySelector("#canvas3D");

export const GraphType = {
    SCATTER: "scatter3d",
    MESH: "mesh3d",
    SURFACE: "surface"
};

const CURRENT_ALGORITHM_NAME = "Perceptron";

/**
 * RENDER
 */
export let x=[];
export let y=[];
export let z=[];
export let pointsArray3D = [];

export const render3DPoints = (pointsArray3D, type, colorScale = false) => {
    const x = [];
    const y = [];
    const z = [];

    pointsArray3D.map(p =>{
        x.push(p.x);
        y.push(p.y);
        z.push(p.z);
    });

    return ({
        opacity: 0.8,
        // mode: 'lines+markers',
        type: type,
        mode: "markers",
        marker: {
            size: 2,
            color: '#9FCC7F'
        },
        x: x,
        y: y,
        z: z,
        // intensity: [0, 0.33, 0.66, 1],
        // colorscale: [
        //     [0, 'rgb(255, 0, 0)'],
        //     [0.5, 'rgb(0, 255, 0)'],
        //     [1, 'rgb(0, 0, 255)']
        // ],
        scene: "scene",
        name: "Current function"
    });
};


export const renderSurfaceData = (pointsArray3D, size) => {
    const z_data = Create2DArrayWithFill(size,0);


    pointsArray3D.forEach(p => {
        // console.log(p.x, p.y);
        z_data[p.x][p.y] = p.z
    });

    // console.log("Z_DATA", z_data);

    function getData() {
        var arr = [];
        for(let i=0;i<20;i++)
            arr.push(Array(3).fill().map(() => Math.random()))
        return arr;
    }

    return [{
        z: z_data,
        type: 'surface',
        colorscale: [
            [0, 'rgb(2, 138, 176)'],
            [0.5, 'rgb(0, 122, 47)'],
            [1, 'rgb(176, 176, 0)']
        ],
    }];
};


export const generateGraphFromFunction = (N_points, func) => {
    let pointsArray = [];

    for(let i = -N_points; i < N_points; i++){
        for(let j = -N_points; j < N_points;j++){
            x.push(i);
            y.push(j);
            z.push(func([i,j]));

            pointsArray.push(new Point3D(i,j,func([i,j])));
        }
    }
    return pointsArray;
};


let layout_3d = {
    margin: {t:0, l:0, b:0},
    opacity: 0.8,
    width: 800,
    height: 800,
    scene: {
        // aspectratio: {
        //     x: 1, y: 0.7, z: 1,
        // },
        // xaxis: {
        //     range: [-2, 2],
        // },
        // yaxis: {
        //     range: [-2, 2],
        // },
        // zaxis: {
        //     range: [-1, 2],
        // },
        camera: {
            // eye: {
            //     x: 1.88,
            //     y: -2.12,
            //     z: 0.96
            // }
            eye: {
                x: 0.8,
                y: -2.0,
                z: 0.5
            }
        }
    }
};

export const getData = (pointsArray, graphType, colorScale = false) => {
    let data_3d = [
        render3DPoints(pointsArray, graphType, colorScale)
    ];
    return data_3d;
};

// RENDER
export const render = (data) => Plotly.newPlot(CANVAS, data, layout_3d, {showSendToCloud: true});
export const renderCustom = (canvas, data, layout) => Plotly.newPlot(canvas, data, layout);

export const updateCanvas = (canvas, data, layout) => Plotly.update(canvas,data, layout);

export function animate(points) {
    Plotly.animate(CANVAS, {
        data: [{
            opacity: 1,
            mode: "markers",
            marker: {size: 2},
            // color:'rgb(033,255,100)',
            // type: 'scatter3d',
            x: points.map(p => p.x),
            y: points.map(p => p.y),
            z: points.map(p => p.z),
            name: CURRENT_ALGORITHM_NAME
        }]
    }, {
        transition: {
            duration: 500,
            // easing: 'cubic-in'
            // redraw: false
        }
    });
    // requestAnimationFrame(animateAnnealing);
}

export function animateDimensional(points) {
    Plotly.animate(CANVAS, {
        data: [{
            opacity: 1,
            mode: "markers",
            marker: {size: 2},
            // color:'rgb(033,255,100)',
            // type: 'scatter3d',
            x: points.map(p => p.x),
            y: points.map(p => p.y),
            z: points.map(p => p.fitness),
            name: CURRENT_ALGORITHM_NAME
        }]
    }, {
        transition: {
            duration: 500,
            // easing: 'cubic-in'
            // redraw: false
        }
    });
    // requestAnimationFrame(animateAnnealing);
}