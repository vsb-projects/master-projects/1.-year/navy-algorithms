
export const Create2DArray = (rows) => {
    let arr = [];
    for (let i = 0;i < rows;i++) {
        arr[i] = [];
    }
    return arr;
};

export const Create2DArrayXY = (rows, columns) => {
    let arr = new Array(columns);
    for (let i = 0;i < columns;i++) {
        arr[i] = new Array(rows);
    }
    return arr;
};

export const Create2DArrayWithFill = (rows, fillValue) => {
    let arr = [];
    for (let i = 0;i <= rows;i++) {
        arr[i] = Array(rows).fill(fillValue);
    }
    return arr;
};

export const copyObject = (src) => {
    return Object.assign({},src);
};

export function randomFromInterval(min, max) {
    return Math.random() * (max - min + 1) + min;
}

export function random_normal(min, max, skew = 1) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = random_normal(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return Math.round(num);
}


// export function shuffleArray(currentArray, shuffleTimes, routes, startCity, endCity) {
//     for(let k = 0; k < shuffleTimes;k++) {
//         const array = [...currentArray];
//         for (let i = array.length - 1; i > 0; i--) {
//             const j = Math.floor(Math.random() * (i + 1));
//             [array[i], array[j]] = [array[j], array[i]];
//         }
//
//         // array.unshift(startCity);
//         // array.push(endCity);
//
//         const route = new Route(array);
//         route.calculateDistance();
//         routes.push(route);
//     }
// }