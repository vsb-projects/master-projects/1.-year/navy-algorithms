import "../css/bootstrap.min.css";
import "../css/styles.css";
import "../images/hourglass.svg";
// import "./Perceptron";
// import "./SimpleNeuralNetwork_XOR";
// import "./HopfieldNetwork";
// import "./L_systems";
// import "./IFS";
// import "./MandelbrotSet";
// import "./FractalLandscapes";
// import "./DoublePendulum";

import {ifs_reset, IFS_START} from "./IFS";
import {ls_reset, LS_START} from "./L_systems";
import {ms_reset, MS_START} from "./MandelbrotSet";
import { fl_reset, FRACTAL_LANDSCAPES_START } from "./FractalLandscapes";
import { bd_reset, BIFURCATION_DIAGRAM_START } from "./LogisticMap";
import { dp_reset, DP_START } from "./DoublePendulum";
import { ff_reset, FF_START } from "./ForestFire"


function load(algorithm, container) {
    loading.style.display = "flex";

    setTimeout(() => new Promise((resolve, reject) => {
        resolve(algorithm())
    }).then(res => {
        loading.style.display = "none";
        container.style.display = "block";
    }), 500)
}
//
//
const loading = document.getElementById("loading");
const ifsSwitchButton = document.getElementById("ifsSwitch");
const lsSwitchButton = document.getElementById("lsSwitch");
const msSwitchButton = document.getElementById("msSwitch");
const flSwitchButton = document.getElementById("flSwitch");
const bdSwitchButton = document.getElementById("bdSwitch");
const dpSwitchButton = document.getElementById("dpSwitch");
const ffSwitchButton = document.getElementById("ffSwitch");

const ifsContainer = document.getElementById('ifsContainer');
const lsContainer = document.getElementById('lsContainer');
const msContainer = document.getElementById('msContainer');
const flContainer = document.getElementById('flContainer');
const bdContainer = document.getElementById('bdContainer');
const dpContainer = document.getElementById('dpContainer');
const ffContainer = document.getElementById('ffContainer');


window.onload = () => {
    document.getElementById("body").style.opacity = 1;
    load(FF_START,ffContainer);
};


lsSwitchButton.addEventListener('click', e => {
    ifsSwitcher(false);
    msSwitcher(false);
    flSwitcher(false);
    bdSwitcher(false);
    dpSwitcher(false);
    ffSwitcher(false);

    lsSwitcher(true);
});


ifsSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    msSwitcher(false);
    flSwitcher(false);
    bdSwitcher(false);
    dpSwitcher(false);
    ffSwitcher(false);

    ifsSwitcher(true);
});

msSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    ifsSwitcher(false);
    flSwitcher(false);
    bdSwitcher(false);
    dpSwitcher(false);
    ffSwitcher(false);

    msSwitcher(true);
});

flSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    ifsSwitcher(false);
    msSwitcher(false);
    bdSwitcher(false);
    dpSwitcher(false);
    ffSwitcher(false);

    flSwitcher(true);
});

bdSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    ifsSwitcher(false);
    msSwitcher(false);
    flSwitcher(false);
    dpSwitcher(false);
    ffSwitcher(false);

    bdSwitcher(true);
});

dpSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    ifsSwitcher(false);
    msSwitcher(false);
    flSwitcher(false);
    bdSwitcher(false);
    ffSwitcher(false);

    dpSwitcher(true);
});

ffSwitchButton.addEventListener('click', e => {
    lsSwitcher(false);
    ifsSwitcher(false);
    msSwitcher(false);
    flSwitcher(false);
    bdSwitcher(false);
    dpSwitcher(false);

    ffSwitcher(true);
});




const lsSwitcher = (on = true) => {
    if(on){
        lsSwitchButton.classList.add("active");
        load(LS_START, lsContainer);
    } else {
        lsSwitchButton.classList.remove("active");
        lsContainer.style.display = 'none';
        ls_reset();
    }
};

const ifsSwitcher = (on = true) => {
    if(on){
        ifsSwitchButton.classList.add("active");
        load(IFS_START, ifsContainer);
    } else {
        ifsSwitchButton.classList.remove("active");
        ifsContainer.style.display = 'none';
        ifs_reset();
    }
};

const msSwitcher = (on = true) => {
    if(on){
        msSwitchButton.classList.add("active");
        load(MS_START, msContainer);
    } else {
        msSwitchButton.classList.remove("active");
        msContainer.style.display = 'none';
        ms_reset();
    }
};

const flSwitcher = (on = true) => {
    if(on){
        flSwitchButton.classList.add("active");
        load(FRACTAL_LANDSCAPES_START, flContainer);
    } else {
        flSwitchButton.classList.remove("active");
        flContainer.style.display = 'none';
        fl_reset();
    }
};

const bdSwitcher = (on = true) => {
    if(on){
        bdSwitchButton.classList.add("active");
        load(BIFURCATION_DIAGRAM_START, bdContainer);
    } else {
        bdSwitchButton.classList.remove("active");
        bdContainer.style.display = 'none';
        bd_reset();
    }
};

const dpSwitcher = (on = true) => {
    if(on){
        dpSwitchButton.classList.add("active");
        load(DP_START, dpContainer);
    } else {
        dpSwitchButton.classList.remove("active");
        dpContainer.style.display = 'none';
        dp_reset();
    }
};

const ffSwitcher = (on = true) => {
    if(on){
        ffSwitchButton.classList.add("active");
        load(FF_START, ffContainer);
    } else {
        ffSwitchButton.classList.remove("active");
        ffContainer.style.display = 'none';
        ff_reset();
    }
};
