import {FractalSquare} from "./models/FractalSquare";
import {Point3D} from "./models/Point";
import {getData, GraphType, renderCustom, renderSurfaceData} from "./utils/render3DGraph";
import {emptyInputValidation, emptyInputValidationWithRange} from "./utils/uiUtils";


/**
 * CONFIG
 */
let roughness = 2;
let perturbation = 50;
let numberOfIteration = 5;
let size = 8;

let InitFractalSquare = new FractalSquare(new Point3D(0,size), new Point3D(size,size), new Point3D(size,0), new Point3D(0,0)); // pocatecni ctverec

let fractalSquareArray = [InitFractalSquare]; // pole ctvercu


/**
 * FRACTAL LANDSCAPES ALGORITHM
 */
const FractalLandscapesAlgorithm = () => {

    let pointsArray = [];

    for(let i = 0; i < numberOfIteration; i++) {

        let newFractalSquareArray = [];
        // v kazde iteraci prochazeni vsech soucasnych ctvercu a rozdelovani na 4 nove ctverce
        for(let fq of fractalSquareArray){
            newFractalSquareArray.push(...fq.splitToSquaresAndPerturb(perturbation));
        }
        // nahrazeni pole ctvercu nove vzniklym polem ctvercu
        fractalSquareArray = [...newFractalSquareArray];

        // snizeni perturbace o hodnotu hrubosti
        perturbation -= roughness;
    }

    // prevod Fraktalnich ctvercu na jednotlive body pro vykresleni
    for(let fq of fractalSquareArray){
        pointsArray.push(...fq.getPoints());
    }

    return pointsArray;
};





/**
 * RENDER
 */
let canvasHeight = window.innerHeight-85;
let canvasWidth = window.innerWidth * 0.85;
const flCanvas = document.getElementById("flCanvas");
const flLoading = document.getElementById("flLoading");

const flLayout = {
    margin: {t:0, l:0, b:0},
    opacity: 0.8,
    width: canvasWidth,
    height: canvasHeight,
    scene: {
        camera: {
            eye: {
                x: 0.8,
                y: -2.0,
                z: 0.5
            }
        }
    }
};


export const FRACTAL_LANDSCAPES_START = (graphType = GraphType.SURFACE) => {
    InitFractalSquare = new FractalSquare(new Point3D(0,size), new Point3D(size,size), new Point3D(size,0), new Point3D(0,0));
    fractalSquareArray = [InitFractalSquare];

    new Promise((resolve, reject) => {
        resolve(FractalLandscapesAlgorithm())
    }).then(res => {
       flLoading.style.display = "none";
       flCanvas.style.display = "block";
       renderCustom(flCanvas, renderSurfaceData(res, size), flLayout);
    });
};

export const fl_reset = () => {
    roughness = 2;
    perturbation = 50;
    numberOfIteration = 5;
    size = 8;
    InitFractalSquare = new FractalSquare(new Point3D(0,size), new Point3D(size,size), new Point3D(size,0), new Point3D(0,0)); // pocatecni ctverec
    fractalSquareArray = [InitFractalSquare];

    epochsInput.value = numberOfIteration;
    sizeInput.value = size;
    perturbationInput.value = perturbation;
    roughnessInput.value = roughness;
};

/**
 * DOM Selectors and functions
 */
const epochsInput = document.getElementById("flEpochsInput");
const sizeInput = document.getElementById("flSizeInput");
const perturbationInput = document.getElementById("flPertInput");
const roughnessInput = document.getElementById("flRoughInput");
const applyButton = document.getElementById("flApply");

epochsInput.value = numberOfIteration;
sizeInput.value = size;
perturbationInput.value = perturbation;
roughnessInput.value = roughness;

epochsInput.addEventListener('input', e => {
    if(emptyInputValidationWithRange(e,0,11)) {
        numberOfIteration = parseInt(e.target.value);
    } else {
        epochsInput.value = numberOfIteration;
    }
});

sizeInput.addEventListener('input', e => {
    if(emptyInputValidationWithRange(e,5,100)) {
        size = parseInt(e.target.value);
    } else {
        sizeInput.value = size;
    }
});

perturbationInput.addEventListener('input', e => {
    if(emptyInputValidationWithRange(e,0,50)) {
        perturbation = parseInt(e.target.value);
    } else {
        perturbationInput.value = perturbation;
    }
});

roughnessInput.addEventListener('input', e => {
    roughness = Number(e.target.value);
});

applyButton.addEventListener('click', e => {
    if(size && roughness) {
        flCanvas.style.display = "none";
        flLoading.style.display = "flex";
        setTimeout(() => FRACTAL_LANDSCAPES_START(), 1000);
    } else {
        alert("Please set all values!");
    }
});