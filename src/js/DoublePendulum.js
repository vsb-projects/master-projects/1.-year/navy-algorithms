import p5 from 'p5';

/**
 * CONFIG
 */
// Kyvadlo 1
let l1 = 200; // rameno kyvadla
let m1 = 30; // velikost prumeru kyvadla
let angle1 = 0; // uhel kyvadla
let velocity1 = 0; //
// Kyvadlo 2
let l2 = 200; // rameno kyvadla
let m2 = 30; // velikost prumeru kyvadla
let angle2 = 0; // uhel kyvadla
let velocity2 = 0;

let g = 1; // gravitace

// X a Y pocatecni promenne
let px = -1;
let py = -1;
let cx, cy;

// buffer pro vykresleni
let buffer;

/**
 * DOUBLE PENDULUM CHAOTIC MOTION ALGORITHM
 */
const DoublePendulumAlgorithm = (p5) => {
    p5.imageMode(p5.CORNER);
    p5.image(buffer, 0, 0, p5.width, p5.height);

    // Vypocet akcelerace prvniho kyvadla
    let theta1Up = (- m2 * g * Math.sin(angle1 - 2 * angle2)) + (-g * (2 * m1 + m2) * Math.sin(angle1)) + (- 2 * Math.sin(angle1 - angle2) * m2) * (Math.pow(velocity2,2) * l2 + Math.pow(velocity1,2) * l1 * Math.cos(angle1 - angle2));
    let theta1Down = l1 * (2 * m1 + m2 - m2 * Math.cos(2 * angle1 - 2 * angle2));
    let acceleration1 = theta1Up / theta1Down;

    // Vypocet akcelerace druheho kyvadla
    let theta2Up = (2 * Math.sin(angle1 - angle2)) * ((Math.pow(velocity1,2) * l1 * (m1 + m2)) + ((m1 + m2) * g * Math.cos(angle1)) + (Math.pow(velocity2,2) * l2 * m2 * Math.cos(angle1 - angle2)));
    let theta2Down = l2 * (2 * m1 + m2 - m2 * Math.cos(2 * angle1 - 2 * angle2));
    let acceleration2 = theta2Up / theta2Down;

    // presunuti kyvadla do zacatku kyvadla a vykresleni
    p5.translate(cx, cy);
    p5.stroke(0);
    p5.strokeWeight(2);

    // Aktualizace souradnic prvniho kyvadla
    let x1 = l1 * Math.sin(angle1);
    let y1 = l1 * Math.cos(angle1);

    // Aktualizace souradnic druheho kyvadla
    let x2 = x1 + l2 * Math.sin(angle2);
    let y2 = y1 + l2 * Math.cos(angle2);

    // Vykresleni prvniho kyvadla
    p5.line(0, 0, x1, y1);
    p5.fill(0);
    p5.ellipse(x1, y1, m1, m1);

    // Vykresleni druheho kyvadla
    p5.line(x1, y1, x2, y2);
    p5.fill(0);
    p5.ellipse(x2, y2, m2, m2);

    // Aktulizace rychlosti a uhlu kyvadel
    velocity1 += acceleration1;
    velocity2 += acceleration2;
    angle1 += velocity1;
    angle2 += velocity2;

    // Aktualizace vykreslovaciho bufferu
    buffer.stroke(0);
    if (p5.frameCount > 1) {
        buffer.line(px, py, x2, y2);
    }
    px = x2;
    py = y2;
};

/**
 * RENDER
 *
 * NASTAVENI VYKRESLOVACI KNIHOVNY P5
 */
let P5;
let canvasWidth = window.innerWidth;
let canvasHeight = window.innerHeight;

export const sketch = p5 => {
    canvasWidth = (canvasWidth * 0.85);
    canvasHeight = (canvasHeight - 85);

    window.p5 = p5;

    // Setup funkce
    // ======================================
    p5.setup = () => {
        p5.createCanvas(canvasWidth, canvasHeight);

        p5.pixelDensity(1);
        // Nastaveni pocatecnich hodnot
        angle1 = p5.PI / 2;
        angle2 = p5.PI / 2;
        cx = (canvasWidth * 0.85) / 2;
        cy = canvasHeight / 3.5;
        buffer = p5.createGraphics(p5.width, p5.height);
        buffer.background("#fff");
        buffer.translate(cx, cy);
    };

    // Vykreslovaci funkce
    // ======================================
    p5.draw = () => {
        DoublePendulumAlgorithm(p5);
    };

};












/**
 * DOM Selectors and functions
 */
const arm1RangeInput = document.getElementById("dpArm1Range");
const arm1LabelValue = document.getElementById("dpArm1Value");
const arm2RangeInput = document.getElementById("dpArm2Range");
const arm2LabelValue = document.getElementById("dpArm2Value");
const pendulum1RangeInput = document.getElementById("dpPend1Range");
const pendulum1LabelValue = document.getElementById("dpPend1Value");
const pendulum2RangeInput = document.getElementById("dpPend2Range");
const pendulum2LabelValue = document.getElementById("dpPend2Value");

const applyButton = document.getElementById("dpApply");

arm1RangeInput.value = l1;
arm1LabelValue.innerText = l1.toString();
arm2RangeInput.value = l2;
arm2LabelValue.innerText = l2.toString();
pendulum1RangeInput.value = m1;
pendulum1LabelValue.innerText = m1.toString();
pendulum2RangeInput.value = m2;
pendulum2LabelValue.innerText = m2.toString();


arm1RangeInput.addEventListener('input', e => {
    arm1LabelValue.innerText = e.target.value;
    l1 = Number(e.target.value);
});
arm2RangeInput.addEventListener('input', e => {
    arm2LabelValue.innerText = e.target.value;
    l2 = Number(e.target.value);
});
pendulum1RangeInput.addEventListener('input', e => {
    pendulum1LabelValue.innerText = e.target.value;
    m1 = Number(e.target.value);
});
pendulum2RangeInput.addEventListener('input', e => {
    pendulum2LabelValue.innerText = e.target.value;
    m2 = Number(e.target.value);
});

applyButton.addEventListener('click', e => {
    restart();
});

const restart = () => {
    P5.remove();
    P5 = new p5(sketch, document.getElementById("dp"));
};

export const dp_reset = () => {
    if(P5 !== undefined) {
        P5.remove();
    }
    // Kyvadlo 1
    l1 = 200; // rameno kyvadla
    m1 = 30; // velikost prumeru kyvadla
    angle1 = 0; // uhel kyvadla
    velocity1 = 0; //
    // Kyvadlo 2
    l2 = 200; // rameno kyvadla
    m2 = 30; // velikost prumeru kyvadla
    angle2 = 0; // uhel kyvadla
    velocity2 = 0;

    g = 1; // gravitace

    // X a Y pocatecni promenne
    px = -1;
    py = -1;
    cx = 0, cy = 0;

    // buffer pro vykresleni
    buffer = null;
};

export const DP_START = () => {
    P5 = new p5(sketch, document.getElementById("dp"))
};