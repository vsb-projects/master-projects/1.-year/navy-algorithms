class Neuron {

    constructor(weights = [], bias, learningRate) {
        this.net = 0;
        this.out = 0;
        this.bias = bias;
        this.weights = weights;
        this.learningRate = learningRate;
        this.error = 0.0;
    }

    netSumFunction(inputValues = []) {
        let result = 0;
        for(let x = 0; x < this.weights.length; x++) {
            result += inputValues[x]*this.weights[x];
        }
        result += this.bias;
        this.net = result;
    }

    outSigmoidFunction() {
        this.out = (Math.pow(Math.E, this.net))/(Math.pow(Math.E, this.net) + 1);
        return this.out;
    }

    calculateNeuronStats(inputValues = []) {
        this.netSumFunction(inputValues);
        return this.outSigmoidFunction();
    }

    updateWeights(error) {
        this.error = error;
        for(let i = 0; i < this.weights.length; i++) {
            this.weights[i] = this.weights[i] - this.learningRate * error * this.out * this.net;
        }
    }
}

export default Neuron;