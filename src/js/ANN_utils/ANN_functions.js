export const signum = (x) => {
    if(x >= 1){
        return 1;
    } else if(x <= -1) {
        return -1;
    } else {
        return 0;
    }
};

export const vectorSignum = (vector) => {
  for(let i = 0; i < vector.length; i++){
      vector[i] = signum(vector[i]);
  }
  return vector;
};