class Layer {

    constructor(neurons = []) {
        this.neurons = neurons;
        // this.totalLayerError = 0;
        this.totalNeuronsError = 0;
        this.totalErrorNeuronArray = [];
    }

    calculateOutputError(targetValue) {
            let error = 0;
            for(let neuron of this.neurons){
                error = (Math.pow((Number(targetValue) - Number(neuron.out)),2))/2;
                // error = Number(targetValue) - Number(neuron.out);
                neuron.updateWeights(error);
                // this.totalLayerError += error;
                this.totalErrorNeuronArray.push(neuron.error);
                this.totalNeuronsError += Number(neuron.error);
            }
            return this.totalNeuronsError;
    }

    calculateNormalError(previousError) {
        for(let neuron of this.neurons){
            neuron.updateWeights(previousError);
            // this.totalLayerError += previousError;
            this.totalErrorNeuronArray.push(neuron.error);
            this.totalNeuronsError = neuron.error;
        }
        return this.totalNeuronsError;
    }

    run(inputValues = []){
        const outValues = [];
        for(let i  = 0; i < this.neurons.length; i++) {
            outValues.push(this.neurons[i].calculateNeuronStats(inputValues));
        }
        return outValues;
    }

    // outputLayerRun(inputValues = [], targetValue) {
    //     const outputValue = this.run(inputValues);
    //     if(outputValue ===)
    // }
}

export default Layer;














export const valueCheck = (inputValues, output, finalOutput) => {
    return inputValues[0] === 1 && inputValues[1] === 1 ? finalOutput[0] : output;
};