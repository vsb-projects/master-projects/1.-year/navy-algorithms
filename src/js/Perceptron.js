import Plotly from 'plotly.js-dist';
import Trace from "./models/Trace";
import {Create2DArray} from "./utils/utilFunctions";
import Point from "./models/Point";
import {animateEdges, generatePoints, render2DGraph} from "./utils/render2DGraph";
let CANVAS = document.querySelector('#canvas');


class Perceptron {

    constructor(weights, bias, learningRate, mainFunction) {
        this.weights = weights;
        this.bias = bias;
        this.learningRate = learningRate;
        this.mainFunction = mainFunction;
    }

    // Error calculation
    calculateError(pointArr, res, resGuess) {
        let Error = Number(res) - Number(resGuess);

        // w = w + Error * input * learningRate;
        for(let x = 0; x < pointArr.length; x++) {
            this.weights[x] = this.weights[x] + Error * pointArr[x] * this.learningRate;
        }
        this.bias = this.bias + Error * this.learningRate;
    };

    // Activation function
    signumFunction(pointArr) {
        let result = 0;
        for(let x = 0; x < pointArr.length; x++) {
            result += pointArr[x]*this.weights[x];
        }
        result += this.bias;

        if(result >= 1){
            result = 1;
        } else if(result <= -1) {
            result = -1;
        } else {
            result = 0;
        }

        // return x*w1 + y*w2 + bias;
        return result;
    };

    // Learn
    learn(pointsArray, epochs) {
        for(let i = 0; i < epochs; i++){
            for(let point of pointsArray){

                let guess = this.signumFunction(point.arr);
                let funcRes = this.mainFunction(point.x);

                if(funcRes === point.y){
                    this.calculateError(point.arr, 0, guess);
                } else if(funcRes < point.y){
                    this.calculateError(point.arr, 1, guess);
                } else {
                    this.calculateError(point.arr, -1, guess);
                }
            }
        }

        console.log("PERCEPTRON learning finished");
    }

    // Classify
    classifyPoint(point) {
        return this.signumFunction(point.arr);
    }
}


/**
 * Perceptron neural network
 */
const numberOfPoints = 100;
const pointsRange = 200;
const epochs = 100;

// Points array
// const pointsArray = Create2DArray(numberOfPoints);
let pointsArray = [];

// Weights and bias
let weights = [];
let w1 = 0.2;
let w2 = 0.4;
weights.push(w1,w2);
let bias = 0.5;
//Constants
const learningRate = 0.1;


// Functions
const mainFunction = (x) => {
    return 4*x - 5;
};


/**
 * ALGORITHM
 */
const PerceptronANN = () => {

    let pointsAbove = [];
    let pointsOnLine = [];
    let pointsBelow = [];

    pointsArray = generatePoints(numberOfPoints, pointsRange);

    // let pointsTrace = new Trace('Origin points','markers', 'scatter', pointsArray, {size: 15});

    // Line
    let linePoint1 = new Point(0, mainFunction(0), "0");
    let linePoint2 = new Point(50, mainFunction(50), "1");
    let mainFunctionLineTrace = new Trace("y = 4 * x - 5", 'lines', 'scatter', [linePoint1, linePoint2]);

    // render2DGraph(pointsTrace, 'Perceptron neural network', 1900, 1000);
    render2DGraph([mainFunctionLineTrace], 'Perceptron neural network', 1900, 1000);


    // Init perceptron
    const perceptron = new Perceptron(weights, bias, learningRate, mainFunction);

    // Learn Perceptron
    perceptron.learn(pointsArray, epochs);

    // Classify points by Perceptron
    // let guess = perceptron.classifyPoint(pointsArray[0]);
    // console.log(`x: ${pointsArray[0].x}, y: ${pointsArray[0].y}`, guess);

    for(let point of pointsArray) {
        let guess = perceptron.classifyPoint(point);
        switch (guess) {
            case 1: {
                pointsAbove.push(point);
                break;
            }
            case 0: {
                pointsOnLine.push(point);
                break;
            }
            case -1: {
                pointsBelow.push(point);
                break;
            }
        }
    }

    let abovePointsTrace = new Trace("Above line points", 'markers', 'scatter', pointsAbove, {size: 15});
    let onLinePointsTrace = new Trace("On line points", 'markers', 'scatter', pointsOnLine, {size: 15});
    let belowPointsTrace = new Trace("Below line points", 'markers', 'scatter', pointsBelow, {size: 15});

    // render2DGraph([new Trace("Classified point", 'markers', 'scatter', [pointsArray[0]], {size: 20})], 'Perceptron neural network', 1900, 1000);
    render2DGraph(CANVAS,[abovePointsTrace, onLinePointsTrace, belowPointsTrace], 'Perceptron neural network', 1900, 1000);


    // animateRoutes(x,y);
    // render(trace);
    // render(startFinishTrace);
};


PerceptronANN();
