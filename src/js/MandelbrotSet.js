import p5 from 'p5';

/**
 * CONFIG
 */
let epochs = 50;
let w_scale = 5;
let h_scale;
let m = 15;

/**
 * MANDELBROT SET ALGORITHM
 */
const MandelbrotSetAlgorithm = (p5) => {
    // Nastaveni vysky a sirky na minus polovinu
    const xmin = -w_scale/2;
    const ymin = -h_scale/2;

    // osa x jde z xmin do xmax
    const xmax = xmin + w_scale;
    // osa y jde z ymin do ymax
    const ymax = ymin + h_scale;

    // Vypocet hodnoty s kterou inkrementujeme pozici x,y kazdeho pixelu
    const dx = (xmax - xmin) / (p5.width);
    const dy = (ymax - ymin) / (p5.height);

    // Zacatek z y
    let y = ymin;
    for (let j = 0; j < p5.height; j++) {
        // Zacatek z x
        let x = xmin;
        for (let i = 0; i < p5.width; i++) {
            // Testovani zda pri iteraci rovnice z = z^2 + cm smeruje z do nekonecna
            let a = x;
            let b = y;
            let n = 0;
            while (n < epochs) {
                const twoab = 2.0 * a * b;
                a = Math.pow(a,2) - Math.pow(b,2) + x;
                b = twoab + y;
                // Pokud se dosahne naseho nastaveneho nekonecna tak konec
                if (p5.dist(a, b, 0, 0) > m) {
                    break;
                }
                n++;
            }

            // Obarveni kazdeho pixelu dle toho jak dlouho mu trva dostat se do nekonecna - pokud se nedostane tak se obarvi nacerno
            const pix = (i+j*p5.width)*4;
            const norm = p5.map(n, 0, epochs, 0, 1);
            let bright = p5.map(p5.sqrt(norm), 0, 1, 0, 255);
            if (n == epochs) {
                bright = 0;
            } else {
                p5.pixels[pix + 0] = bright;
                p5.pixels[pix + 1] = bright;
                p5.pixels[pix + 2] = bright;
                p5.pixels[pix + 3] = 255;
            }
            x += dx;
        }
        y += dy;
    }
    p5.updatePixels();
};






////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RENDER
 *
 * NASTAVENI VYKRESLOVACI KNIHOVNY P5
 */
let P5;
let canvasWidth = window.innerWidth;
let canvasHeight = window.innerHeight;

export const sketch = p5 => {
    canvasWidth = p5.windowWidth;
    canvasHeight = p5.windowHeight;

    window.p5 = p5;

    // Setup funkce
    // ======================================
    p5.setup = () => {
        p5.createCanvas(canvasWidth, canvasHeight);
        p5.pixelDensity(1);
        p5.noLoop();
    };

    // Vykreslovaci funkce
    // ======================================
    p5.draw = () => {
        p5.background(0);
        p5.loadPixels();
        h_scale = (w_scale * p5.height) / p5.width;
        MandelbrotSetAlgorithm(p5);
    };

};


/**
 * DOM Selectors and functions
 */
const epochsInput = document.getElementById("msEpochsInput");
const scaleInput = document.getElementById("msScaleInput");
const infinityInput = document.getElementById("msInfinityInput");
const applyButton = document.getElementById("msApply");

epochsInput.value = epochs;
scaleInput.value = w_scale;
infinityInput.value = m;

epochsInput.addEventListener('input', e => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            epochs = parseInt(e.target.value);
        } else {
            alert("Set number value, por favor.");
        }
    }
});

scaleInput.addEventListener('input', e => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            w_scale = parseInt(e.target.value);
        } else {
            alert("Set number value, por favor.");
        }
    }
});

infinityInput.addEventListener('input', e => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            m = parseInt(e.target.value);
        } else {
            alert("Set number value, por favor.");
        }
    }
});

const restart = () => {
    P5.remove();
    P5 = new p5(sketch, document.getElementById("ms"));
};

applyButton.addEventListener('click', e => {
    restart();
});

export const ms_reset = () => {
    if(P5 !== undefined) {
        P5.remove();
    }
    epochs = 50;
    w_scale = 4;
    m = 15;
};

export const MS_START = () => {
    P5 = new p5(sketch, document.getElementById("ms"))
};