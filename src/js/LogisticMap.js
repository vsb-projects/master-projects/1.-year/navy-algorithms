import Point from "./models/Point";
import {GraphType} from "./utils/render3DGraph";
import Trace from "./models/Trace";
import {render2DGraph, TraceModeTypes, TraceTypeTypes} from "./utils/render2DGraph";


/**
 * CONFIG
 */
let numberOfIteration = 150;
let x = 0.1;
let r_init = 2;
let r_incrementValue = 0.005;

let pointsArray = [new Point(r_init,x)];


// dana funkce pro vypocet logisticke mapy
const logisticMapEquation = (x,r) => {
    return Number(r) * Number(x) * (1 - Number(x));
};


/**
 * BIFURCATION DIAGRAM ALGORITHM
 */
const BifurcationDiagram = () => {
    // postupne inkrementovani hodnoty r on danou nastavenou hodnotu
    for(let r = Number(r_init); r < 4; r += r_incrementValue) {
        // pro kazdou novou hodnotu r se vyhodnoti rovnice dle poctu iteraci a ziskane x z rovnice se spolu s r vlozi do pole pro vykresleni
        for (let i = 0; i < numberOfIteration; i++) {
            x = logisticMapEquation(x, r);
            pointsArray.push(new Point(r, x));
        }
    }
};









/**
 * RENDER
 */
const bdCanvas = document.getElementById("bdCanvas");
const bdLoading = document.getElementById("bdLoading");
let canvasWidth = window.innerWidth * 0.85;
let canvasHeight = window.innerHeight-85;


export const BIFURCATION_DIAGRAM_START = (graphType = GraphType.SCATTER) => {
    new Promise((resolve, reject) => {
        resolve(BifurcationDiagram())
    }).then(res => {
        let trace = new Trace("Bifurcation trace", TraceModeTypes.markers, TraceTypeTypes.scatter, pointsArray, {size: 2});

        bdLoading.style.display = "none";
        bdCanvas.style.display = "block";
        render2DGraph(bdCanvas, [trace], "Bifurcation diagram", canvasWidth, canvasHeight);
    });
};

export const bd_reset = () => {
    let numberOfIteration = 300;
    let x = 0.1;
    let r_init = 2;
    let r_incrementValue = 0.005;

    let pointsArray = [new Point(r_init,x)];
};


/**
 * DOM Selectors and functions
 */
const epochsRangeInput = document.getElementById("bdEpochsRange");
const epochsLabelValue = document.getElementById("bdEpochsValue");
// const XRangeInput = document.getElementById("bdXRange");
// const XLabelValue = document.getElementById("bdXValue");
const RRangeInput = document.getElementById("bdRRange");
const RLabelValue = document.getElementById("bdRValue");
const RInRangeInput = document.getElementById("bdRInRange");
const RInLabelValue = document.getElementById("bdRInValue");
const applyButton = document.getElementById("bdApply");

epochsRangeInput.value = numberOfIteration;
epochsLabelValue.innerText = numberOfIteration;
// XRangeInput.value = x;
// XLabelValue.innerText = x;
RRangeInput.value = r_init;
RLabelValue.innerText = r_init;
RInRangeInput.value = r_incrementValue;
RInLabelValue.innerText = r_incrementValue;


epochsRangeInput.addEventListener('input', e => {
    epochsLabelValue.innerText = e.target.value;
    numberOfIteration = Number(e.target.value);
});

// XRangeInput.addEventListener('input', e => {
//     XLabelValue.innerText = e.target.value;
//     x = Number(e.target.value);
// });

RRangeInput.addEventListener('input', e => {
    RLabelValue.innerText = e.target.value;
    r_init = Number(e.target.value);
});

RInRangeInput.addEventListener('input', e => {
    RInLabelValue.innerText = e.target.value;
    r_incrementValue = Number(e.target.value);
});

applyButton.addEventListener('click', e => {
    pointsArray = [];
    bdCanvas.style.display = "none";
    bdLoading.style.display = "flex";
    setTimeout(() => BIFURCATION_DIAGRAM_START(), 500);
});