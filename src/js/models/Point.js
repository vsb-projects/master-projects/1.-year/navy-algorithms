// Individual
export default class Point {
    constructor(x,y){
        this.x = x;
        this.y = y;
        this.arr = [x,y];
    }

    countDistance(point) {
        let xDis = Math.abs(this.x - point.x);
        let yDis = Math.abs(this.y - point.y);

        return Math.sqrt((Math.pow(xDis,2) + Math.pow(yDis,2)));
    }
}

export class Point3D{
    constructor(x,y,z = 0.0){
        this.x = x;
        this.y = y;
        this.z = z;
        this.fitness = 0.0;
    }

    checkSame(anotherPoint) {
        if(this.x === anotherPoint.x){
            if(this.y === anotherPoint.y){
                if(this.z === anotherPoint.z){
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    valuateSelf(func) {
        this.z = func([this.x, this.y]);
    }
}

