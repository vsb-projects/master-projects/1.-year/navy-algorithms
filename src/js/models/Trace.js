
export default class Trace{

    constructor(name, mode, type, points = [], marker = {}, line = {}){
        this.name = name;
        this.mode = mode;
        this.type = type;
        this.points = points;
        this.marker = marker;
        this.line = line;
    }

    addPoint(point){
        this.points.push(point);
    }

    renderTrace(){
        let x = [];
        let y = [];
        this.points.forEach(point => {
           x.push(point.x);
           y.push(point.y);
        });

        return ({
            name: this.name,
            x: x,
            y: y,
            mode: this.mode,
            type: this.type,
            marker: this.marker,
            line: this.line
        });
    }
}