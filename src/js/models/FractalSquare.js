import {Point3D} from "./Point";
import {randomFromInterval} from "../utils/utilFunctions";

/**
 * Fraktalni ctverec
 */
export class FractalSquare {

    // Jednotlive 3D body ctverce
    constructor(lu,ru,rd,ld) {
        this.lu = lu; // levy horni bod
        this.ru = ru; // pravy horni bod
        this.rd = rd; // pravy dolni bod
        this.ld = ld; // levy dolni bod
    }

    // Rozdeleni tohoto ctverce na 4 nove ctverce a perturbace osy z novych 5 vzniklych bodu
    splitToSquaresAndPerturb(perturbation) {
        let topPoint = new Point3D(Math.round(Math.abs(this.lu.x - this.ru.x) / 2),this.lu.y, randomFromInterval(0, perturbation));
        let bottomPoint = new Point3D(Math.round(Math.abs(this.ld.x - this.rd.x) / 2), this.ld.y, randomFromInterval(0, perturbation));
        let leftPoint = new Point3D(this.lu.x, Math.round(Math.abs(this.lu.y - this.ld.y)) / 2, randomFromInterval(0, perturbation));
        let rightPoint = new Point3D(this.ru.x, Math.round(Math.abs(this.ru.y - this.rd.y) / 2), randomFromInterval(0, perturbation));
        let middlePoint = new Point3D(Math.round(Math.abs(leftPoint.x - rightPoint.x) / 2), Math.round(Math.abs(topPoint.y - bottomPoint.y) / 2), randomFromInterval(0, perturbation));

        // vyvvoreni a vraceni nove vzniklych 4 ctvercu
        let leftTopFractalSquare = new FractalSquare(this.lu, topPoint, middlePoint, leftPoint);
        let rightTopFractalSquare = new FractalSquare(topPoint,this.ru, rightPoint, middlePoint);
        let rightBottomFractalSquare = new FractalSquare(middlePoint, rightPoint, this.rd, bottomPoint);
        let leftBottomFractalSquare = new FractalSquare(leftPoint, middlePoint, bottomPoint, this.ld);

        return [leftTopFractalSquare, rightTopFractalSquare, rightBottomFractalSquare, leftBottomFractalSquare];
    };

    getPoints() {
        return [this.lu, this.ru, this.rd, this.ld];
    }
}