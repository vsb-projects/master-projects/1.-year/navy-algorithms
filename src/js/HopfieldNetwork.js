import {renderHeatmap, renderGridOnCanvas} from "./utils/renderHeatmap";
import numjs from "numjs";
import {vectorSignum} from "./ANN_utils/ANN_functions";

import "../css/styles.css";
import {randomFromInterval} from "./utils/utilFunctions";

// CANVASES
let CANVAS1 = document.querySelector('#canvas1');
let CANVAS2 = document.querySelector('#canvas2');
let CANVAS3 = document.querySelector('#canvas3');
const inputCanvases = [CANVAS1, CANVAS2, CANVAS3];

let CANVAS_DEF1 = document.querySelector('#canvasDef1');
let CANVAS_DEF2 = document.querySelector('#canvasDef2');
let CANVAS_DEF3 = document.querySelector('#canvasDef3');
const deformedCanvases = [CANVAS_DEF1, CANVAS_DEF2, CANVAS_DEF3];

let CANVAS_RES1 = document.querySelector('#canvasRes1');
let CANVAS_RES2 = document.querySelector('#canvasRes2');
let CANVAS_RES3 = document.querySelector('#canvasRes3');
let resultCanvases = [CANVAS_RES1, CANVAS_RES2, CANVAS_RES3];

let CANVAS_ASYNC_RES1 = document.querySelector('#canvasAsyncRes1');
let CANVAS_ASYNC_RES2 = document.querySelector('#canvasAsyncRes2');
let CANVAS_ASYNC_RES3 = document.querySelector('#canvasAsyncRes3');
let resultAsyncCanvases = [CANVAS_ASYNC_RES1, CANVAS_ASYNC_RES2, CANVAS_ASYNC_RES3];

// INPUT MATRIXES
const inputResultMatrix1 = [
    [0,1,1,1,0],
    [0,1,0,1,0],
    [0,1,1,1,0],
    [0,1,0,1,0],
    [0,1,1,1,0],
];
const inputResultMatrix2 = [
    [0,1,1,1,0],
    [0,1,0,0,0],
    [0,1,1,1,0],
    [0,0,0,1,0],
    [0,1,1,1,0],
];
const inputResultMatrix3 = [
    [1,0,0,0,1],
    [0,1,0,1,0],
    [0,0,1,0,0],
    [0,1,0,1,0],
    [1,0,0,0,1],
];
const inputMatrixes = [inputResultMatrix1, inputResultMatrix2, inputResultMatrix3];

// DEFORMED MATRIXES
const deformedAttemptMatrix1 = [
    [0,1,0,1,0],
    [0,1,0,1,0],
    [0,0,1,0,0],
    [0,1,0,1,0],
    [0,1,0,1,0],
];
const deformedAttemptMatrix2 = [
    [0,1,1,1,0],
    [0,0,0,0,0],
    [0,1,0,1,0],
    [0,0,0,0,0],
    [0,1,1,1,0],
];
const deformedAttemptMatrix3 = [
    [1,0,0,0,1],
    [0,1,0,1,0],
    [0,0,1,0,0],
    [0,0,0,0,0],
    [1,0,0,0,1],
];
const deformedMatrixes = [deformedAttemptMatrix1, deformedAttemptMatrix2, deformedAttemptMatrix3];

// FUNCTIONS
/**
 * Funkce na vytvoreni vektoru z dane matice
 * @param matrix
 * @returns {[]} - vektor
 */
const makeMeAVectorFromMatrix = (matrix) => {
    let vector = [];
    for(let m of matrix){
        vector = vector.concat(m);
    }
    return vector;
};

/**
 * Funkce na prevedeni bud vsech -1 na 0 nebo 0 na -1 dle potreby
 * @param vector
 * @param toZeros - podminka zda se maji prevest 0 jinak se prevedou -1
 * @returns {*} - vrati upraveny vektor
 */
const makeMeVectorChanged = (vector, toZeros = false) => {
    let index;
    while(vector.includes(0)){
        if (toZeros){
            index = vector.indexOf(-1);
        } else {
            index = vector.indexOf(0);
        }
        if(index !== -1){
            toZeros ? vector[index] = 0 : vector[index] = -1;
        }
    }
    return vector;
};


/**
 * Funkce na vypocet outer produktu
 * @param v - vektor
 * @returns {[]} - matice
 */
const outerProduct = (v) => {
    let resultMatrix = [];

    for(let i = 0; i < v.length; i++){
        let resultVector = [];
        for(let j = 0; j < v.length; j++) {
            resultVector.push(v[i]*v[j]);
        }
        resultMatrix.push(resultVector);
    }
    return resultMatrix;
};

/**
 * Funkce pro vykresleni danych matic na dane HTML canvasy
 * @param matrixArray
 * @param canvasArray
 */
const renderMatrixes = (matrixArray, canvasArray) => {
    for(let i = 0; i < matrixArray.length; i++) {
        renderGridOnCanvas(matrixArray[i], canvasArray[i]);
    }
};

/**
 * Asynchronni metoda klasifikace deformovanych vzorku
 * @param defMatrix - deformovana matice
 * @returns {NdArray} - NDpole rozpoznane matice
 */
const asynchronous = (defMatrix) => {

    // matice se prevede na vektor
    let defVector = numjs.array(makeMeAVectorFromMatrix(defMatrix));

    // dle poctu epoch se provadi asynchronni klasifikace
    for(let i = 0; i < asyncEpochs; i++){
        // vzdy se nahodne vybere slopec
        let columnNumber = Math.floor(randomFromInterval(0,24));

        // sloupec se vyjme z drive vypocitane matice vah
        let selectedColumn = weightedMatrix.tolist().map((value,index) => value[columnNumber]);
        // sloupec se transponuje
        selectedColumn = numjs.array(selectedColumn).reshape(selectedColumn.length,1);

        // vypocita se skalar pomoci dot produktu deformovaneho vektoru a vybraneho sloupce
        let scalar = numjs.dot(defVector, selectedColumn);
        // vyhodinoti se signum funkce
        scalar = vectorSignum(scalar.tolist())[0];

        // pro na index sloupce se do deformovaneho vektoru vlozi dany skalar
        defVector = defVector.tolist();
        defVector[columnNumber] = scalar;

        defVector = numjs.array(defVector);
    }

    // nakonec prevede a vrati jiz klasifikovana deformovana matice
    return defVector.reshape(defMatrix.length, defMatrix.length);
};


/**
 * Hopfield network
 */
// matice vah
let weightedMatrix = numjs.zeros([Math.pow(inputResultMatrix1.length,2), Math.pow(inputResultMatrix1.length,2)]);
// pocet epoch pro asynchronni metodu
let asyncEpochs = 200;

/**
 * ALGORITHM
 */
const HopfieldNetwork = () => {
    // Render sample example patterns
    renderMatrixes(inputMatrixes, inputCanvases);
    // Render deformed samples patterns
    renderMatrixes(deformedMatrixes, deformedCanvases);

    // Trenovaci cast - prochazi se vsechny vzorove martice
    for(let m of inputMatrixes) {
        // z kazde matice se nejprve vytvori vektor a prevedou se 0 na -1
        let inputVector = makeMeVectorChanged(makeMeAVectorFromMatrix(m));
        // provede se vypocet dane matice - odecte ve outer produkt vektoru a jednokovy vektor
        let finalMatrix = numjs.subtract(numjs.array(outerProduct(inputVector)), numjs.identity(inputVector.length));
        // aktualizuje se matice vah dle vvpoctene matice
        weightedMatrix.add(finalMatrix, false);
    }

    console.log(weightedMatrix.toJSON());


    // RUN - prochazi se vsechny deformovane matice
    /**
     * SYNCHRONNI METODA
     */
    for(let i = 0; i < deformedMatrixes.length; i++) {
        // vytvori se vektor z deformovane matice
        let defVector = numjs.array(makeMeAVectorFromMatrix(deformedMatrixes[i]));
        // provede se dot produkt matice vah a deformovaneho vektoru
        let resultVector = numjs.dot(weightedMatrix, defVector);
        // vypocte se signum funkce
        resultVector = vectorSignum(resultVector.tolist());
        // vytvori se vysledna klasifikovana matice
        let resultMatrix = numjs.array(resultVector).reshape(deformedMatrixes[i].length, deformedMatrixes[i].length).tolist();

        renderGridOnCanvas(resultMatrix, resultCanvases[i]);
    }

    /**
     * ASYNCHRONNI METODA
     */
    for(let i = 0; i < deformedMatrixes.length; i++) {
        // metoda popsana vyse
        let resultAsyncMatrix = asynchronous(deformedMatrixes[i]).tolist();
        renderGridOnCanvas(resultAsyncMatrix, resultAsyncCanvases[i]);
    }
};

HopfieldNetwork();

