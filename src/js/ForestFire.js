import p5 from 'p5';

/**
 * CONFIG
 */
let rectSize = 30;
let board,columns,rows;
let p = 0.05; // pravdepodobnost rustu noveho stromu
let f = 0.001; // pravdepodobnost ze strom zacne horet
let frameRate = 10; // vykreslovaci frekvence

// Typy poli
const FieldTypes = {
  EMPTY: "EMPTY",
  TREE: "TREE",
  FIRE: "FIRE"
};

// Pole pro vykresleni
class Field {
    constructor(x,y,size, type = FieldTypes.EMPTY) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.type = type;
    }
}


/**
 * FOREST FIRE ALGORITHM
 */
const ForestFireAlgorithm = () => {

    let newBoard = new Array(columns);
    for (let i = 0; i < columns; i++) {
        newBoard[i] = new Array(rows);
    }


    // Prochazeni celeho prostredi a zmena typu poli dle podminek
    for ( let i = 0; i < columns;i++) {
        for (let j = 0; j < rows; j++) {
            newBoard[i][j] = board[i][j];

            switch (board[i][j].type) {
                // pokud je pole hori tak se zmeni na prazdne
                case FieldTypes.FIRE: {
                    newBoard[i][j].type = FieldTypes.EMPTY;
                    break;
                }
                // pokud je pole strom a v jeho okoli je ohen nebo dle dane pravdepodobnosti, tak se zacne horet taky
                case FieldTypes.TREE: {
                    if(board[i === 0 ? 0 : i-1][j === 0 ? 0 : j-1].type === FieldTypes.FIRE ||
                        board[i === 0 ? 0 : i-1][j].type === FieldTypes.FIRE ||
                        board[i === 0 ? 0 : i-1][(j+1) === rows ? j : j+1].type === FieldTypes.FIRE ||
                        board[i][j === 0 ? 0 :  j-1].type === FieldTypes.FIRE ||
                        board[i][(j+1) === rows ? j : j+1].type === FieldTypes.FIRE ||
                        board[(i+1) === columns ? i : i+1][j === 0 ? 0 : j-1].type === FieldTypes.FIRE ||
                        board[(i+1) === columns ? i : i+1][j].type === FieldTypes.FIRE ||
                        board[(i+1) === columns ? i : i+1][(j+1) === rows ? j : j+1].type === FieldTypes.FIRE
                    ) {
                        newBoard[i][j].type = FieldTypes.FIRE;
                    } else {
                        if(Math.random() <= f){
                            newBoard[i][j].type = FieldTypes.FIRE;
                        }
                    }
                    break;
                }
                // pokud je pole prazdne tak dle dane pravdepodobnosti muze vyrust novy strom
                case FieldTypes.EMPTY: {
                    if(Math.random() <= p){
                        newBoard[i][j].type = FieldTypes.TREE;
                    }
                    break;
                }
            }
        }
    }

    board = newBoard;
};


/**
 * Funkce pro inicializaci prostredi
 */
const initBoard = (p5) => {
    // Urceni poctu sloupcu a radku
    columns = p5.floor(canvasWidth / rectSize);
    rows = p5.floor(canvasHeight / rectSize);
    p5.background("#fff");

    // Vytvoreni dvou rozmerneho pole prostredi
    board = new Array(columns);
    for (let i = 0; i < columns; i++) {
        board[i] = new Array(rows);
    }

    // Inicializace prostedi stromy
    for ( let i = 0; i < columns;i++) {
        for ( let j = 0; j < rows;j++) {
            // board[i][j] = new Field(i * w, j * w, w-1, FieldTypes[randomProperty(FieldTypes)]);
            board[i][j] = new Field(i * rectSize, j * rectSize, rectSize-1, FieldTypes.TREE);
            const {
                x,y,size
            } = board[i][j];
            p5.stroke(0);
            p5.rect(x,y,size,size);
        }
    }
};

/**
 * Funkce pro prekresleni prostredi
 */
const renderBoard = (p5) => {
    p5.frameRate(frameRate);

    const getFill = (type) => {
        switch (type) {
            case FieldTypes.EMPTY: {
                return p5.fill(255);
            }
            case FieldTypes.TREE: {
                return p5.fill("#005F0D");
            }
            case FieldTypes.FIRE: {
                return p5.fill("#710800")
            }
        }
    };
    for ( let i = 0; i < columns;i++) {
        for (let j = 0; j < rows; j++) {
            const {
                x, y, size, type
            } = board[i][j];
            getFill(type, p5);

            p5.stroke(0);
            p5.rect(x, y, size, size);
        }
    }
};















/**
 * RENDER
 *
 * NASTAVENI VYKRESLOVACI KNIHOVNY P5
 */
let P5;
let canvasWidth = window.innerWidth;
let canvasHeight = window.innerHeight;

export const sketch = p5 => {
    canvasWidth = (canvasWidth * 0.85);
    canvasHeight = (canvasHeight - 85);

    window.p5 = p5;

    // Setup funkce
    // ======================================
    p5.setup = () => {
        p5.createCanvas(canvasWidth, canvasHeight);
        initBoard(p5);
    };

    // Vykreslovaci funkce
    // ======================================
    p5.draw = () => {
        ForestFireAlgorithm(p5);
        renderBoard(p5);
    };

};



/**
 * DOM Selectors and functions
 */
const frameRateRangeInput = document.getElementById("ff-frameRateRange");
const frameRateLabelValue = document.getElementById("ff-frameRateValue");
const fireProbRangeInput = document.getElementById("ff-fireProbRange");
const fireProbLabelValue = document.getElementById("ff-fireProbValue");
const treeProbRangeInput = document.getElementById("ff-treeProbRange");
const treeProbLabelValue = document.getElementById("ff-treeProbValue");
const fieldSizeRangeInput = document.getElementById("ff-fieldSizeRange");
const fieldSizeLabelValue = document.getElementById("ff-fieldSizeValue");

const applyButton = document.getElementById("ffApply");

frameRateRangeInput.value = frameRate;
frameRateLabelValue.innerText = frameRate.toString();
fireProbRangeInput.value = f;
fireProbLabelValue.innerText = f.toString();
treeProbRangeInput.value = p;
treeProbLabelValue.innerText = p.toString();
fieldSizeRangeInput.value = rectSize;
fieldSizeLabelValue.innerText = rectSize.toString();


frameRateRangeInput.addEventListener('input', e => {
    frameRateLabelValue.innerText = e.target.value;
    frameRate = Number(e.target.value);
});
fireProbRangeInput.addEventListener('input', e => {
    fireProbLabelValue.innerText = e.target.value;
    f = Number(e.target.value);
});
treeProbRangeInput.addEventListener('input', e => {
    treeProbLabelValue.innerText = e.target.value;
    p = Number(e.target.value);
});
fieldSizeRangeInput.addEventListener('input', e => {
    fieldSizeLabelValue.innerText = e.target.value;
    rectSize = Number(e.target.value);
});

applyButton.addEventListener('click', e => {
    restart();
});

const restart = () => {
    P5.remove();
    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;
    board = [];
    columns = 0;
    rows = 0;
    P5 = new p5(sketch, document.getElementById("ff"));
};

export const ff_reset = () => {
    if(P5 !== undefined) {
        P5.remove();
    }
    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;
    rectSize = 40;
    board = [];
    columns = 0;
    rows = 0;
    p = 0.05;
    f = 0.001;
    frameRate = 10;
};

export const FF_START = () => {
    P5 = new p5(sketch, document.getElementById("ff"))
};
