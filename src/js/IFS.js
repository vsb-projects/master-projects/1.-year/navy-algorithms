import {Create2DArray, randomFromInterval} from "./utils/utilFunctions";
import {getData, GraphType, render, renderCustom, updateCanvas} from "./utils/render3DGraph";
import {Point3D} from "./models/Point";

/**
 * CONFIG
 */
const probability = 0.25; // nastaveni pravdepodobnosti pro vyhodnocovani

let firstModelEpochs = 20000;
let secondModelEpochs = 20000;

const fernModelOne = [ // transforamce prvniho modelu
    [0.00,0.00,0.01,0.00,0.26,0.00,0.00,0.00,0.05,0.00,0.00,0.00],
    [0.20,-0.26,-0.01,0.23,0.22,-0.07,0.07,0.00,0.24,0.00,0.00,0.00],
    [-0.25,0.28,0.01,0.26,0.24,-0.07,0.07,0.00,0.24,0.00,0.22,0.00],
    [0.85,0.04,-0.01,-0.04,0.85,0.09,0.00,0.08,0.84,0.00,0.80,0.00]
];

const fernModelTwo = [ // transformace druheho modelu
    [0.05,0.00,0.00,0.00,0.60,0.00,0.00,0.00,0.05,0.00,0.00,0.00],
    [0.45,-0.22,0.22,0.22,0.45,0.22,-0.22,0.22,-0.45,0.00,1.00,0.00],
    [-0.45,0.22,-0.22,0.22,0.45,0.22,0.22,-0.22,0.45,0.00,1.25,0.00],
    [0.49,-0.08,0.08,0.08,0.49,0.08,0.08,-0.08,0.49,0.00,2.00,0.00]
];

let position = [[1],[1],[1]]; // pocatecni pozice

let firstModelFinalArray = []; // pole vystupnich bodu prvniho modelu
let secondModelFinalArray = []; // pole vystupnich bodu druheho modelu

const makeMeMatrixAndTransformationFromArray = (array,mLen, mCut) => { // funkce pro prevedeni transformaci na matici a vektor
    let matrix = Create2DArray(mCut);
    let insideMat = [];
    let trans = [];

    let cutPos = 1;
    let row = 0;
    for(let i = 0; i < mLen;i++){
        insideMat.push(array[i]);
       if(i === (mCut*cutPos-1)){
           matrix[row] = [...insideMat];
           cutPos++;
           row++;
           insideMat.length = 0;
       }
    }
    for(let j = mLen; j < array.length; j++) {
        trans.push([array[j]]);
    }

    return [matrix,trans];
};

const multiplyMatrixes = (m1, m2) => { // funkce pro nasobeni matic
    let result = [];
    for (let i = 0; i < m1.length; i++) {
        result[i] = [];
        for (let j = 0; j < m2[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < m1[0].length; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
};

const addMatrixes = (m1,m2) => { // funkce pro scitani matic
    const result = [];
    for(let i = 0; i < m1.length; i++){
        let row = [];
        for(let j = 0; j < m1[i].length; j++){
            row.push((Number(m1[i][j]) + Number(m2[i][j])));
        }
        result.push(row);
    }
    return result;
};


/**
 * IFS ALGORITHM
 * @param epochs - pocet epoch vyhodnocovani
 * @param model - model s danymi transformacemi
 * @param finalArray - vyledne pole bodu k vykresleni
 * @constructor
 */
const IFS = (epochs, model, finalArray) => {

    // spusteni dle poctu epoch
    for(let i = 0; i < epochs; i++){
        // prochazeni transformaci daneho modelu
        for(let j = 0; j < model.length; j++){
            // vygenerovani nahodneho cisla v intervalu <0,1> pokud je mensi nebo rovno dane pravdepodobnosti tak je transformace aplikovana
            if(Math.random() <= probability){
                // vyvoreni matice a vektoru z vybrane transformace
                const [matrix,trans] = makeMeMatrixAndTransformationFromArray(model[j], 9, 3);

                // vynasobeni ziskane matice aktualni pozici bodu
                position = multiplyMatrixes(matrix,position);

                // vyhdnoceni nove pozice bodu souctem aktualni pozice a transformacniho vektoru
                position = addMatrixes(position, trans);

                // vlozeni aktualni pozice do pole bodu k vykresleni
                finalArray.push(new Point3D(position[0][0],position[1][0], position[2][0]));
            }
        }
    }
};





/////////////////////////////////////////////////////////////////////////////////////////////
/**
 * RENDER
 */
let canvasWidth = window.innerWidth;
let canvasHeight = window.innerHeight;

const firstModelCanvas = document.getElementById("ifsFirstModelCanvas");
const secondModelCanvas = document.getElementById("ifsSecondModelCanvas");
const firstModelEpochsInput = document.getElementById("firstModelEpochsInput");
const secondModelEpochsInput = document.getElementById("secondModelEpochsInput");
const firstModelApplyButton = document.getElementById("firstModelApply");
const secondModelApplyButton = document.getElementById("secondModelApply");

firstModelEpochsInput.value = firstModelEpochs;
secondModelEpochsInput.value = secondModelEpochs;

firstModelEpochsInput.addEventListener('input', e => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            firstModelEpochs = parseInt(e.target.value);
        } else {
            alert("Set number value, por favor.");
            // firstModelEpochsInput.value = firstModelEpochs;
        }
    }
});
secondModelEpochsInput.addEventListener('input', e => {
    if(e.target.value.length > 0) {
        if (parseInt(e.target.value)) {
            secondModelEpochs = parseInt(e.target.value);
        } else {
            alert("Set number value, por favor.");
            // secondModelEpochsInput.value = secondModelEpochs;
        }
    }
});

firstModelApplyButton.addEventListener('click', e => {
    if(firstModelEpochs > 0){
        firstModelFinalArray = [];
        IFS(firstModelEpochs,fernModelOne, firstModelFinalArray);
        renderCustom(firstModelCanvas,getData(firstModelFinalArray, GraphType.SCATTER), firstModelLayout);
    }
});

secondModelApplyButton.addEventListener('click', e => {
    if(secondModelEpochs > 0){
        secondModelFinalArray = [];
        IFS(secondModelEpochs,fernModelTwo, secondModelFinalArray);
        renderCustom(secondModelCanvas,getData(secondModelFinalArray, GraphType.SCATTER), secondModelLayout);
    }
});

const firstModelLayout = {
    margin: {t:0, l:0, b:0},
    opacity: 0.8,
    width: canvasWidth/3,
    height: canvasWidth/3,
    scene: {
        camera: {
            eye: {
                x: 0.8,
                y: -2.0,
                z: 0.5
            }
        }
    }
};

const secondModelLayout = {
    margin: {t:0, l:0, b:0},
    opacity: 0.8,
    width: canvasWidth/3,
    height: canvasWidth/3,
    scene: {
        camera: {
            eye: {
                x: 0,
                y: 0.6,
                z: -1.8
            }
        }
    }
};

export const ifs_reset = () => {
    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;

    firstModelEpochs = 20000;
    secondModelEpochs = 20000;
    position = [[1],[1],[1]];

    firstModelFinalArray = [];
    secondModelFinalArray = [];
};

export const IFS_START = (graphType = GraphType.SCATTER) => {
    IFS(firstModelEpochs,fernModelOne, firstModelFinalArray);
    IFS(secondModelEpochs,fernModelTwo, secondModelFinalArray);
    renderCustom(firstModelCanvas,getData(firstModelFinalArray, graphType), firstModelLayout);
    renderCustom(secondModelCanvas,getData(secondModelFinalArray, graphType), secondModelLayout);
};

// IFS_START(GraphType.SCATTER);