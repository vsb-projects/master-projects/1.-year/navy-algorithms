import p5 from 'p5';

/**
 * CONFIG
 */
let x, y; // soucasna pozice vykreslovani
let currentAngle = 0; // momentalni uhel pro vykreslovani
let memoryState = []; // zasobnik stavu
let canvasWidth = window.innerWidth;
let canvasHeight = window.innerHeight-35;

let whereinstring = 0; // k postupnemu vykrelovani
let step = 5; // velikost cary 1 kroku vykresleni
let outputString = ""; // finalni retezec k vykresleni
let transpose = false; // preklopeny o 90 stupnu

/**
 * PATTERNS
 */
const first = {
    name: "First",
    axiom: "F+F+F+F",
    rules: [
        ["F", "F+F-F-FF+F+F-F"]
    ],
    angle: 90,
    n: 3,
    xStart: canvasWidth/3,
    yStart: canvasHeight/3,
    step: 5,
    transpose: false
};

const second = {
    name: "Second",
    axiom: "F++F++F",
    rules: [
        ["F", "F+F--F+F"]
    ],
    angle: 60,
    n: 4,
    xStart: canvasWidth/3,
    yStart: canvasHeight/3,
    step: 6,
    transpose: false
};

const third = {
    name: "Third",
    axiom: "F",
    rules: [
        ["F", "F[+F]F[-F]F"]
    ],
    angle: 180/7,
    n: 4,
    xStart: canvasWidth/2-150,
    yStart: canvasHeight-50,
    step: 7,
    transpose: true
};

const fourth = {
    name: "Fourth",
    axiom: "F",
    rules: [
        ["F", "FF+[+F-F-F]-[-F+F+F]"]
    ],
    angle: 180/8,
    n: 4,
    xStart: canvasWidth/2-150,
    yStart: canvasHeight-50,
    step: 7,
    transpose: true
};

const KochSnowflake = {
    name: "Koch snowflake",
    axiom: "F++F++F",
  rules: [
      ["F", "F-F++F-F"]
  ],
  angle: 60,
  n: 4,
    xStart: canvasWidth/4,
    yStart: canvasHeight/3,
    step: 6,
    transpose: false
};

const SerpinskiTriangle = {
    name: "Serpinski triangle",
    axiom: "FXF--FF--FF",
    rules: [
        ["F", "FF"],
        ["X", "--FXF++FXF++FXF--"]
    ],
    angle: 60,
    n: 5,
    xStart: canvasWidth/3,
    yStart: canvasHeight/2,
    step: 6,
    transpose: false
};

const SerpinskiCarpet = {
    name: "Serpinski carpet",
    axiom: "F",
    rules: [
        ["F", "FFF[+FFF+FFF+FFF]"]
    ],
    angle: 90,
    n: 4,
    xStart: canvasWidth/4,
    yStart: canvasHeight/4,
    step: 6,
    transpose: false
};
const SerpinskiCurve = {
    name: "Serpinski curve",
    axiom: "F+XF+F+XF",
    rules: [
        ["X", "XF-F+F-XF+F+XF-F+F-X"]
    ],
    angle: 90,
    n: 4,
    xStart: canvasWidth/3,
    yStart: canvasHeight/4,
    step: 6,
    transpose: false
};

const IceFractal = {
    name: "Ice fractal",
    axiom: "F+F+F+F",
    rules: [
        ["F", "FF+F++F+F"]
    ],
    angle: 90,
    n: 4,
    xStart: canvasWidth/3,
    yStart: canvasHeight/4,
    step: 6,
    transpose: false
};
const McWorter = {
    name: "McWorter",
    axiom: "F-F-F-F-F",
    rules: [
        ["F", "F-F-F++F+F-F"]
    ],
    angle: 72,
    n: 4,
    xStart: canvasWidth/2,
    yStart: canvasHeight/5*4,
    step: 5,
    transpose: false
};
const Bush1 = {
    name: "Bush 1",
    axiom: "F",
    rules: [
        ["F", "F[+FF][-FF]F[-F][+F]F"]
    ],
    angle: 180/5,
    n: 5,
    xStart: canvasWidth/2-150,
    yStart: canvasHeight-50,
    step: 7,
    transpose: true
};
const Bush2 = {
    name: "Bush 2",
    axiom: "X",
    rules: [
        ["F", "FF"],
        ["X", "F[+X]F[-X]+X"]
    ],
    angle: 180/9,
    n: 5,
    xStart: canvasWidth/2-150,
    yStart: canvasHeight-50,
    step: 7,
    transpose: true
};

const patternList = [first,second, third, fourth, KochSnowflake, SerpinskiTriangle, SerpinskiCurve,Bush1, Bush2, IceFractal, McWorter];

/**
 * CURRENT PATTERN
 */
let CURRENT_PATTERN = KochSnowflake;

/**
 * NASTAVENI VYKRESLOVACI KNIHOVNY P5
 */
export const sketch = p5 => {
    canvasWidth = p5.windowWidth;
    canvasHeight = p5.windowHeight;

    window.p5 = p5;

    // Setup funkce
    // ======================================
    p5.setup = () => {
        p5.createCanvas(canvasWidth, canvasHeight);
        p5.background(255);
        p5.stroke(0, 0, 0, 255);

        // Vyhodnoceni vystupniho retezce k vykreselni obrazce dle soucasneho vzorku pomoci Lindenmayerova algoritmu
        outputString = LidenmayerAlgorithm(CURRENT_PATTERN);

        x = CURRENT_PATTERN.xStart;
        y = CURRENT_PATTERN.yStart;

        transpose = CURRENT_PATTERN.transpose;
        step = CURRENT_PATTERN.step;

        whereinstring = 0;
        currentAngle = 0;
    };

    // Vykreslovaci funkce
    // ======================================
    p5.draw = () => {

        if(whereinstring < outputString.length) {
            drawIt(outputString[whereinstring], p5, CURRENT_PATTERN.angle);
            whereinstring++;
        }
        // if (whereinstring > outputString.length-1) whereinstring = 0;
    };
};

/**
 * Funkce pro vlozeni prepisovaciho pravidla misto daneho neterminalu
 */
function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index+1);
}

/**
 * L-system
 * @constructor
 */
const LidenmayerAlgorithm = (pattern) => {
    const {
        axiom,
        rules,
        angle,
        n
    } = pattern;

    console.log(axiom, rules);

    let outputString = axiom;

    // Iterace dle poctu epoch
    for(let i = 0; i < n; i++){
        // prochazeni zvetsujiciho se rezezce pravidel
        for(let j = 0; j < outputString.length; j++) {
            // overeni zda je dany znak na aktualnim indexu prepsatelny nejakym z definovach pravidel
            let rule = rules.filter(f => f[0] === outputString[j])[0];
            if (rule && rule.length > 0) {
                // prepsani daneho neterminalu prepisovacim pravidlem
                outputString = insert(outputString, j, rule[1]);
                // nasataveni pozice aktulniho indexu na dalsi znak za vlozenym pravidlem
                j += rule[1].length-1;
            }
        }
    }

    return outputString;
};


// vykreslovani kroku
function drawIt(k, p5, angle) {

    if (k === 'F') { // prekreslilt rovne
        let x1;
        let y1;
        // uprava uhlu kreseleni
        if(transpose){
            x1 = x - step * Math.sin(p5.radians(currentAngle));
            y1 = y - step * Math.cos(p5.radians(currentAngle));
        } else {
             x1 = x + step * Math.cos(p5.radians(currentAngle));
             y1 = y + step * Math.sin(p5.radians(currentAngle));
        }
        p5.line(x, y, x1, y1); // vykreseni cary

        // update aktualni pozice
        x = x1;
        y = y1;

    } else if (k === '+') {
        currentAngle += angle; // zatocit vlevo
    } else if (k === '-') {
        currentAngle -= angle; // ztocit vpravo
    } else if (k === '[') {
        memoryState.push({ // zapamatovat si aktualni stav do zasobniku
            mX: x,
            mY: y,
            mA: currentAngle
        });
    } else if (k === ']') { // obnovit stav ze zasobniku
        if(memoryState.length !== 0) {
            const { mX, mY, mA } = memoryState.pop();
            x = mX;
            y = mY;
            currentAngle = mA;
        }
    }
}









///////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * UI nasteveni starnky pro vykreseleni stranky, ktere se nevztahuje primo k algorimu - dale bez komentare
 */
const patternsToDrawList = document.getElementById("patternsToDrawList");

let setupOpen = false;
const setupPatternItem = document.getElementById("setupPatternItem");
const setupPatternList = document.getElementById("setupPatternList");

setupPatternItem.addEventListener('click', () => {
   setupOpen = !setupOpen;

   if(setupOpen){
       setupPatternList.classList.add("show");
       setupPatternItem.classList.add("setupActive");
       if (activeElementId) {
           document.getElementById(activeElementId).classList.remove("active");
           document.getElementById(`collapse-${activeElementId}`).classList.remove("show");
       }
   } else {
       setupPatternList.classList.remove("show");
       setupPatternItem.classList.remove("setupActive");
   }
});


let ownAxiom = null;
let ownRuleOne1 = null;
let ownRuleOne2 = null;
let ownRuleTwo1 = null;
let ownRuleTwo2 = null;
let ownN = null;
let ownAngle = null;
let ownX = null;
let ownY = null;

const axiomInput = document.getElementById("axiomInput");
const ruleOneInput1 = document.getElementById("ruleOneInput1");
const ruleOneInput2 = document.getElementById("ruleOneInput2");
const ruleTwoInput1 = document.getElementById("ruleTwoInput1");
const ruleTwoInput2 = document.getElementById("ruleTwoInput2");
const ownNInput = document.getElementById("ownN");
const ownAngleInput = document.getElementById("ownAngle");
const ownXInput = document.getElementById("ownX");
const ownYInput = document.getElementById("ownY");
const renderPatternButton = document.getElementById("renderPattern");

axiomInput.addEventListener('input', e => {
    ownAxiom = e.target.value;
});
ruleOneInput1.addEventListener('input', e => {
    ownRuleOne1 = e.target.value;
});
ruleOneInput2.addEventListener('input', e => {
    ownRuleOne2 = e.target.value;
});
ruleTwoInput1.addEventListener('input', e => {
    ownRuleTwo1 = e.target.value;
});
ruleTwoInput2.addEventListener('input', e => {
    ownRuleTwo2 = e.target.value;
});
ownNInput.addEventListener('input', e => {
    ownN = e.target.value;
});
ownAngleInput.addEventListener('input', e => {
    ownAngle = e.target.value;
});
ownXInput.addEventListener('input', e => {
    ownX = e.target.value;
});
ownYInput.addEventListener('input', e => {
    ownY = e.target.value;
});
renderPatternButton.addEventListener('click', e => {
    if(ownAxiom === null || !ownAxiom.length > 0){
        alert("Prosim zadejte axiom!")
    } else {
        if (((ownRuleOne1 && ownRuleOne1.length > 0 && ownRuleOne2 && ownRuleOne2.length > 0) || (ownRuleTwo1 && ownRuleTwo1.lenght > 0 && ownRuleTwo2 && ownRuleTwo2.length > 0))) {
            const ruleArr = [];
            if (ownRuleOne1 !== null && ownRuleOne2 !== null) {
                ruleArr.push([ownRuleOne1, ownRuleOne2]);
            }
            if (ownRuleTwo1 !== null && ownRuleTwo2 !== null) {
                ruleArr.push([ownRuleTwo1, ownRuleTwo2]);
            }

            if(ownN && ownAngle && ownX && ownY){

            CURRENT_PATTERN = {
                name: "Own pattern",
                axiom: ownAxiom,
                rules: ruleArr,
                angle: Number(ownAngle),
                n: Number(ownN),
                xStart: Number(ownX),
                yStart: Number(ownY),
                step: 5,
                transpose: false
            };

            restart();
            } else {
                alert("Zadejte vsechny hodnoty N - Angle - Start X - Start Y")
            }
        } else {
            alert("Zadejte prosim spravne alespon jedno pravidlo!")
        }
    }
});


const restart = () => {
    P5.remove();
    P5 = new p5(sketch, document.getElementById("p5"));
};

const renderNewPattern = (id) => {
    if (activeElementId) {
        document.getElementById(activeElementId).classList.remove("active");
        document.getElementById(`collapse-${activeElementId}`).classList.remove("show");
    }
    document.getElementById(id).classList.add("active");
    document.getElementById(`collapse-${id}`).classList.add("show");
    activeElementId = id;

    setupOpen = false;
    setupPatternList.classList.remove("show");
    setupPatternItem.classList.remove("setupActive");

    P5.remove();
    P5 = new p5(sketch, document.getElementById("p5"));
};

patternsToDrawList.addEventListener('click', e => {
    const id = e.target.getAttribute("id");

    if(!id.includes("Input")) {

        if(id.includes("Apply")){
            if(id.includes("nApply") && changedNumberOfLoops !== 0){
                CURRENT_PATTERN.n = changedNumberOfLoops;
                changedNumberOfLoops = 0;
                restart();
            } else if(id.includes("posApply")) {
                let posChanged = false;
                if(changedX !== -1) {
                    CURRENT_PATTERN.xStart = changedX;
                    changedX = -1;
                    posChanged = true;
                }
                if(changedY !== -1) {
                    CURRENT_PATTERN.yStart = changedY;
                    changedY = -1;
                    posChanged = true;
                }
                if(posChanged) {
                    restart();
                }
            }
        } else {
            CURRENT_PATTERN = patternList[parseInt(id)];
            renderNewPattern(id);
        }
    }
});

patternsToDrawList.addEventListener('input', e => {
   if(parseInt(e.target.value)){
       const inputId = e.target.getAttribute("id");
       if(inputId.includes("nInput")){
           changedNumberOfLoops = parseInt(e.target.value);
       } else if (inputId.includes("xInput")) {
           changedX = parseInt(e.target.value);
       } else if (inputId.includes("yInput")) {
           changedY = parseInt(e.target.value);
       }
   }
});

let P5;
let activeElementId = null;
let changedNumberOfLoops = 0;
let changedX = -1;
let changedY = -1;

const init = () => {

    patternList.map((pattern, index) => {
        return patternsToDrawList.insertAdjacentHTML('beforeend', `
            <button 
                id="${index}" 
                style="font-weight: bold;" 
                type="button" 
                class="list-group-item collapsed list-group-item-action" 
                data-target="#collapseTwo" 
                aria-expanded="false" 
                aria-controls="collapseTwo"
                >
                    ${pattern.name}
            </button>
            <div id="collapse-${index}" class="collapse">
                <div style="padding: 0.5rem;border: 5px solid #9FCC7F; border-top: 0;border-radius: 0 0 0.7rem 0.7rem;">
                <div style="display: flex; flex-direction: column;">
                    <span><strong>Axiom: </strong> <span style="font-style: italic;">${pattern.axiom}</span></span>
                    <span><strong>Rules: </strong> <span style="font-style: italic;">${pattern.rules.map(r => r[0]+" -> "+r[1]) + ", "}</span></span>
                </div>
                 <label style="font-size: small;margin: 0;">Number of loops:</label>
                <div class="input-group mb-2">
                        <input id="nInput-${index}" type="text" class="form-control form-control-sm" placeholder="${pattern.n}">
                    <div class="input-group-append">
                         <button id="nApply-${index}" class="btn btn-sm btn-outline-success" type="button">Apply</button>
                    </div>
                </div>
                <div style="display: flex; justify-content: space-around;">
                    <label style="font-size: small; margin: 0;">Start X:</label>
                    <label style="font-size: small; margin: 0;">Start Y:</label>
                </div>
                <div style="display: flex;">
                    <div class="input-group-sm mb-2 mr-2">
                        <input id="xInput-${index}" type="text" class="form-control" placeholder="${pattern.xStart}">
                    </div>
                    <div class="input-group-sm mb-2">
                         <input id="yInput-${index}" type="text" class="form-control" placeholder="${pattern.yStart}">
                    </div>
                </div>
                <button id="posApply-${index}" class="btn btn-block btn-sm btn-success" type="button">Apply position</button>
                </div>
            </div>
            `)
    });

    activeElementId = patternList.indexOf(KochSnowflake);
    document.getElementById(activeElementId).classList.add("active");
    P5 = new p5(sketch, document.getElementById("p5"));
};


export const ls_reset = () => {
    if(P5 !== undefined) {
        P5.remove();
    }
    x = 0;
    y = 0;
    currentAngle = 0;
    memoryState = [];
    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;

    whereinstring = 0;
    step = 5;
    outputString = "";
    transpose = false;

    CURRENT_PATTERN = KochSnowflake;

    activeElementId = null;
    changedNumberOfLoops = 0;
    changedX = -1;
    changedY = -1;

    patternsToDrawList.innerHTML = "";
};

/**
 * INIT funkce - start vseho
 */
// init();


export const LS_START = () => {
  init();
};