const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // entry: ['./src/js/Perceptron.js'],
    // entry: ['./src/js/SimpleNeuralNetwork_XOR.js'],
    // entry: ['./src/js/HopfieldNetwork.js'],
    // entry: ['./src/js/L_systems.js'],
    entry: ['./src/js/index.js'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin ({
            filename: 'index.html',
            template: './src/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'ify-loader'
            },
            {
                test: /\.(png|jpg|gif|jpeg|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        // limit: 8000, // Convert images < 8kb to base64 strings
                        name: 'img/[name].[ext]'
                    }
                }]
            },
            {
                test:/\.(s*)css$/,
                use:[
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            url: true,
                        }
                    }
                ],

            },
        ]
    },

};